package com.cejv416.passby;

import com.cejv416.bean.ANumber;

/**
 * Demonstration of the difference between Pass By Value and Pass By Reference
 *
 * @author Ken Fogel
 */
public class PassBy {

    /**
     * Passing a reference to a method
     *
     * @param reference All objects are passed by reference
     */
    public void passByReference(ANumber reference) {
        int n = reference.getTheNumber();
        n *= 3;
        reference.setTheNumber(n);
        System.out.println("Value after change in passByReference()" + reference.getTheNumber());
    }

    /**
     * Passing a value to a method
     *
     * @param value All primitives are passed by value
     */
    public void passByValue(int value) {
        value *= 3;
        System.out.println("Value after change in passByValue()  " + value);
    }

    /**
     * Pass by reference and pass by value
     */
    public void perform() {
        int theNumber;
        theNumber = 8;
        System.out.println("Value before pass by value: " + theNumber);
        passByValue(theNumber);
        System.out.println("Value after the pass by value: " + theNumber);

        ANumber num = new ANumber();
        num.setTheNumber(8);
        System.out.println("Value before pass by reference: " + num.getTheNumber());
        passByReference(num);
        System.out.println("Value after the pass by reference: " + num.getTheNumber());
    }

    /**
     * Where it all begins
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new PassBy().perform();
    }
}
